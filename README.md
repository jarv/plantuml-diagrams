# plantuml-diagrams

```plantuml
rectangle "Frontend HAProxy" as A 
rectangle "Legacy VMs" as B {
  card "git-cny-#" as B1
  card "git-#" as B2
}

rectangle "Kubernetes" as C {
  card "gitlab" as C1
  card "gitlab-cny" as C2
}



A -- C1 : label1
A -- C2 : label2
A -- B1 : label1
A -- B2 : label2
```
